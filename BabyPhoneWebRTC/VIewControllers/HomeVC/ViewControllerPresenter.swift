//
//  ViewControllerPresenter.swift
//  MonitorWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 05/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import Foundation
import UIKit

class ViewControllerPresenter {
    var viewController: ViewControllerProtocol?
    
    init(viewController: ViewControllerProtocol) {
        self.viewController = viewController
    }
}

extension ViewControllerPresenter: ViewControllerPresenterProtocol {
    func willAppear() {
        guard let currentUser = ConcretUser.currentUser() else {
            return
        }
        viewController?.setAvatar(imageName: currentUser.avatar ?? "galery")
    }
    
    func didLoad() {
        viewController?.setPhoneName(phoneName: UIDevice.current.name)
    }
}
