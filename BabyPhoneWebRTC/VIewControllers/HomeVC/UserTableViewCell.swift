//
//  UserTableViewCell.swift
//  BabyPhoneWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 12/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var name: UILabel!
}
