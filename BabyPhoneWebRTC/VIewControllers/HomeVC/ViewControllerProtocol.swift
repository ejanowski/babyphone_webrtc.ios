//
//  ViewControllerProtocol.swift
//  MonitorWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 05/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import Foundation

protocol ViewControllerPresenterProtocol {
    func didLoad()
    func willAppear()
}

protocol ViewControllerProtocol {
    func setPhoneName(phoneName: String)
    func setAvatar(imageName:String)
}
