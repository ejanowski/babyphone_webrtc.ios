//
//  ViewController.swift
//  MonitorWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 05/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import UIKit

class ViewController: UIViewController, Titlable {

    @IBOutlet weak var mLabelPhoneName: UILabel!
    @IBOutlet weak var mAvatarButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var presenter:ViewControllerPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = ViewControllerPresenter(viewController: self)
        presenter?.didLoad()
        displayTitle(title: NSLocalizedString("title", comment: ""))
        
        ConcretUser.createFalseData()
        
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.willAppear()
    }
}

extension ViewController: ViewControllerProtocol {
    func setPhoneName(phoneName: String) {
        mLabelPhoneName.text = phoneName
    }
    
    func setAvatar(imageName: String) {
        mAvatarButton.setImage(UIImage(named: imageName), for: .normal)
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let user = ConcretUser.currentUser() else {
            return 0
        }
        return user.users?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell") as? UserTableViewCell
        if let cell = cell,
            let user = ConcretUser.currentUser(),
            let friend = user.users?[indexPath.row] as? User {
            cell.avatar?.image = UIImage(named: friend.avatar ?? "galery")
            cell.name.text = friend.name
        }
        return cell ?? UITableViewCell()
    }
    
    
}

