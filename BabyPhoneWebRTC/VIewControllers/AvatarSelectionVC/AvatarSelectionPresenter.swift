//
//  AvatarSelectionPresenter.swift
//  MonitorWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 10/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import Foundation
import UIKit

class AvatarSelectionPresenter {
    var emojis = [String]()
    var viewController: AvatarSelectionViewControllerProtocol?
    
    let emojiNumber = 50
    
    init(viewController: AvatarSelectionViewControllerProtocol) {
        self.viewController = viewController
        
        for i in 1...emojiNumber {
            emojis.append("emoji_\(i)")
        }
    }
}

extension AvatarSelectionPresenter: AvatarSelectionPresenterProtocol {
    func selectAvatar(avatar: Int) {
        guard let app = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let user = ConcretUser.currentUser()
        user?.avatar = emojis[avatar]
        app.saveContext()
        viewController?.dismiss()
    }
    
    func selectedEmojiIndex() -> Int {
        let user = ConcretUser.currentUser()
        let imageName = user?.avatar
        
        return emojis.firstIndex(of: imageName ?? "") ?? -1
    }
}
