//
//  AvatarSelectionViewController.swift
//  MonitorWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 10/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import UIKit

class AvatarSelectionViewController: UIViewController, Titlable {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var presenter:AvatarSelectionPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = AvatarSelectionPresenter(viewController: self)
        displayTitle(title: NSLocalizedString("myavatar", comment: ""))
        
//        let indexPath = IndexPath(item: presenter?.selectedEmojiIndex() ?? 0, section: 0)
//        collectionView.scrollToItem(at: indexPath, at: .bottom, animated: true)
    }
    
    @IBAction func onClosePressed(_ sender: Any) {
        dismiss()
    }
    
}

extension AvatarSelectionViewController: AvatarSelectionViewControllerProtocol {
    func dismiss() {
        dismiss(animated: true, completion: nil)
    }
}

extension AvatarSelectionViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.selectAvatar(avatar: indexPath.row)
    }
}

extension AvatarSelectionViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvatarCell", for: indexPath as IndexPath) as? AvatarCollectionViewCell
        
        if cell != nil {
            cell?.imageview.image = UIImage(named: "emoji_\(indexPath.row+1)")
            if presenter?.selectedEmojiIndex() == indexPath.row {
                cell?.imageview.alpha = 1
                cell?.tick.isHidden = false
            } else {
                cell?.imageview.alpha = 0.7
                cell?.tick.isHidden = true
            }
        }
        
        return cell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter?.emojiNumber ?? 0
    }   
}

extension AvatarSelectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width:CGFloat = collectionView.frame.size.width/3.0
        return CGSize(width: width, height: width)
    }
}
