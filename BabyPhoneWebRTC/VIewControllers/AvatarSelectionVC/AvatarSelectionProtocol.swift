//
//  AvatarSelectionProtocol.swift
//  MonitorWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 10/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import Foundation

protocol AvatarSelectionPresenterProtocol {
    func selectAvatar(avatar:Int)
    func selectedEmojiIndex() -> Int
}

protocol AvatarSelectionViewControllerProtocol {
    func dismiss()
}
