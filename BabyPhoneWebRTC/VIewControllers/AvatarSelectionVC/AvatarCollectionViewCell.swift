//
//  AvatarCellCollectionViewCell.swift
//  MonitorWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 11/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import UIKit

class AvatarCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var tick: UIImageView!
}
