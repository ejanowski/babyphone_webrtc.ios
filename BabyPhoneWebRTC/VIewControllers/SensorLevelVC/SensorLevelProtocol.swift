//
//  SensorLevelProtocol.swift
//  BabyPhoneWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 13/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import Foundation

protocol SensorLevelPresenterProtocol {
    func didLoad()
}

protocol SensorLevelViewControllerProtocol {
    func tintElement()
}
