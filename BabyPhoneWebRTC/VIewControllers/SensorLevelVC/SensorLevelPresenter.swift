//
//  SensorLevelPresenter.swift
//  BabyPhoneWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 13/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import Foundation

class SensorLevelPresenter {
    
    var viewController: SensorLevelViewControllerProtocol?
    
    init(viewController: SensorLevelViewControllerProtocol) {
        self.viewController = viewController
    }
}

extension SensorLevelPresenter: SensorLevelPresenterProtocol {
    func didLoad() {
        viewController?.tintElement()
    }
}
