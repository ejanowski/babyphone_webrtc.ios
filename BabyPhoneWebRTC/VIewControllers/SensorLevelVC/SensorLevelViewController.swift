//
//  SensorLevelViewController.swift
//  BabyPhoneWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 13/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import UIKit

class SensorLevelViewController: UIViewController, Titlable {

    var presenter:SensorLevelPresenterProtocol?
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var slider: UISlider!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter = SensorLevelPresenter(viewController: self)
        
        presenter?.didLoad()
        
        displayTitle(title: NSLocalizedString("initialization", comment: ""))
    }

}

extension SensorLevelViewController: SensorLevelViewControllerProtocol {
    func tintElement() {
        progressView.tintColor = UIColor.C1()
        slider.tintColor = UIColor.C1()
    }
}
