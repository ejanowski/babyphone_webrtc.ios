//
//  ConcretUser.swift
//  MonitorWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 11/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import UIKit
import CoreData

class ConcretUser: User {
    public class func currentUser() -> User? {
        guard let user = owner() else {
            return createOwner()
        }
        return user
    }
    
    class func  createOwner() -> User? {
        guard let app = UIApplication.shared.delegate as? AppDelegate,
            let entity = NSEntityDescription.entity(forEntityName: EntityEnum.user.rawValue,
                                                      in: app.persistentContainer.viewContext) else {
            return nil
        }
        let user = User(entity: entity, insertInto: app.persistentContainer.viewContext)

        user.avatar = nil
        user.isOwner = true
        user.uuid = NSUUID().uuidString
        user.name = UIDevice.current.name
        
        app.saveContext()
        
        return owner()
    }
    
    class func  owner() -> User? {
        guard let app = UIApplication.shared.delegate as? AppDelegate,
            let entity = NSEntityDescription.entity(forEntityName: EntityEnum.user.rawValue,
                                                    in: app.persistentContainer.viewContext) else {
                                                        return nil
        }
        let request:NSFetchRequest<User> = User.fetchRequest()
        request.entity = entity
        
        let predicate = NSPredicate(format: "isOwner = %@", NSNumber(booleanLiteral: true))
        
        request.predicate = predicate
        
        var users:[User] = [User]()
        do {
            users = try app.persistentContainer.viewContext.fetch(request)
        } catch {
            print("erreur lors du fetch")
        }
        
        return users.first
    }
    
    class func createFalseData() {
        guard let app = UIApplication.shared.delegate as? AppDelegate,
            let user = ConcretUser.currentUser(),
            let entity = NSEntityDescription.entity(forEntityName: EntityEnum.user.rawValue,
                                                    in: app.persistentContainer.viewContext) else {
                                                        return
        }
        
        let user1 = User(entity: entity, insertInto: app.persistentContainer.viewContext)
        user1.avatar = "emoji_10"
        user1.isOwner = false
        user1.uuid = NSUUID().uuidString
        user1.name = "test1"
        user.addToUsers(user1)
        
        let user2 = User(entity: entity, insertInto: app.persistentContainer.viewContext)
        user2.avatar = "emoji_40"
        user2.isOwner = false
        user2.uuid = NSUUID().uuidString
        user2.name = "test2"
        user.addToUsers(user2)
        
        let user3 = User(entity: entity, insertInto: app.persistentContainer.viewContext)
        user3.avatar = "emoji_30"
        user3.isOwner = false
        user3.uuid = NSUUID().uuidString
        user3.name = "test3"
        user.addToUsers(user3)
        
        let user4 = User(entity: entity, insertInto: app.persistentContainer.viewContext)
        user4.avatar = nil
        user4.isOwner = false
        user4.uuid = NSUUID().uuidString
        user4.name = "test4"
        user.addToUsers(user4)
        
        app.saveContext()
    }
}
