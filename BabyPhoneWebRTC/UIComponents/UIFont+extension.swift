//
//  UIFont+extension.swift
//  MonitorWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 11/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    class func titleFont() -> UIFont {
        return UIFont(name: "Baskerville-BoldItalic", size: 30) ?? UIFont.boldSystemFont(ofSize: 30)
    }
    
    class func subtitleFont() -> UIFont {
        return UIFont(name: "Baskerville-BoldItalic", size: 17) ?? UIFont.boldSystemFont(ofSize: 17)
    }
}
