//
//  Noisable.swift
//  BabyPhoneWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 14/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import Foundation
import UIKit

public protocol Noisable: class {
    func startDetection()
}


public extension Noisable where Self: UIViewController {

}
