//
//  UIColor+extension.swift
//  MonitorWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 10/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    //Blue
    class func C1 () -> UIColor {
        return UIColor(red: 0, green: 210/255.0, blue: 210/255.0, alpha: 1)
    }
    
    //Grey
    class func C2 () -> UIColor {
        return UIColor(red: 40/255.0, green: 40/255.0, blue: 40/255.0, alpha: 1)
    }
    
    //Red
    class func C3 () -> UIColor {
        return UIColor(red: 250/255.0, green: 86/255.0, blue: 85/255.0, alpha: 1)
    }
    
    //light Grey
    class func C4 () -> UIColor {
        return UIColor(red: 180/255.0, green: 180/255.0, blue: 180/255.0, alpha: 1)
    }
    
    public enum ThemedColor: Int {
        case C1 = 1
        case C2 = 2
        case C3 = 3
        case C4 = 4
        
        var color: UIColor {
            switch self {
            case .C1:
                return .C1()
            case .C2:
                return .C2()
            case .C3:
                return .C3()
            case .C4:
                return .C4()
            }
        }
    }
}
