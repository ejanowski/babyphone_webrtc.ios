//
//  Titlable.swift
//  MonitorWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 11/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import Foundation
import UIKit

public protocol Titlable: class {
    
    func displayTitle(title: String)
}

public extension Titlable where Self: UIViewController {
    
    func displayTitle(title: String) {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
        label.text = title
        label.textAlignment = .center
        label.textColor = UIColor.C2()
        label.font = UIFont.titleFont()
        label.minimumScaleFactor = 0.5
        self.navigationItem.titleView = label
        
        customBackButton()
    }
    
    func customBackButton() {
        if self.navigationController?.viewControllers.count ?? 0 > 1 {
        
            let yourBackImage = UIImage(named: "back")
            self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
            self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
            self.navigationController?.navigationBar.backItem?.title = ""
            
            self.navigationController?.navigationBar.tintColor = UIColor.C1()
        }
    }
}

