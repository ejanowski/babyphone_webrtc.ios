//
//  ThemedBarButtonItem.swift
//  MonitorWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 11/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import UIKit

class ThemedBarButtonItem: UIBarButtonItem {
    
    @IBInspectable var imageName: String?
    
    override func awakeFromNib() {
        if let imageName = imageName {
            image = UIImage(named: imageName)
            tintColor = UIColor.C1()
        }
    }
}
