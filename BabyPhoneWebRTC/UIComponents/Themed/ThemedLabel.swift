//
//  ThemedLabel.swift
//  MonitorWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 11/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import UIKit

class ThemedLabel: UILabel {

    @IBInspectable var themedFont: Bool = false
    @IBInspectable var colorFont: Int = -1
    @IBInspectable var localized: Bool = false
    
    override func draw(_ rect: CGRect) {
        if localized {
            text = NSLocalizedString(text ?? "", comment: "")
        }
        if themedFont {
            font = UIFont.subtitleFont()
        }
        if colorFont >= 0 {
            textColor = UIColor.ThemedColor(rawValue: colorFont)?.color
        }
        super.draw(rect)
    }

}
