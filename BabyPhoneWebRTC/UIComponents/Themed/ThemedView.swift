//
//  ThemedView.swift
//  BabyPhoneWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 14/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import UIKit

class ThemedView: UIView {
    @IBInspectable var themedColor: Int = 1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = UIColor.ThemedColor(rawValue: themedColor)?.color
    }
}
