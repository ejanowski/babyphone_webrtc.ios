//
//  BorderButton.swift
//  MonitorWebRTC
//
//  Created by JANOWSKI Emeric [IT-CE] on 10/11/2018.
//  Copyright © 2018 JANOWSKI Emeric [IT-CE]. All rights reserved.
//

import UIKit

class ThemedButton: UIButton {
    
    @IBInspectable var bordered: Bool = false
    @IBInspectable var imageName: String?
    @IBInspectable var localized: Bool = false
    @IBInspectable var themedColor: Int = 1

    override func draw(_ rect: CGRect) {
        let color = UIColor.ThemedColor(rawValue: themedColor)?.color
        setTitleColor(color, for: .normal)
        setTitleColor(color, for: .highlighted)
        if bordered {
            layer.borderWidth = 1
            layer.borderColor = color?.cgColor
        }
        if let imageName = imageName {
            setImage(UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate), for: .normal)
            imageView?.tintColor = color
        }
        
        if localized {
            setTitle(NSLocalizedString(title(for: .normal) ?? "", comment: ""),
                     for: .normal)
        }
        super.draw(rect)
    }

}
